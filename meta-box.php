<?php

$structure = "
  <style>
    .debate-campaign-metabox label {font-weight: bold; width: 30%; display: inline-block}
    @media only screen and (min-width: 800px) {
      .debate-campaign-metabox > div {width: 45%; display: inline-block}
      .debate-campaign-metabox input[type='file'],
      .debate-campaign-metabox select {width: 54%}
    }
    @media only screen and (max-width: 800px) {.debate-campaign-metabox select {display: block}}
  </style>
  <div class='debate-campaign-metabox'>
    <div>
      <label>Status</label>
      *status*
    </div>
    <div>
      <label>Deadline</label>
      *deadline*
    </div>
    <div>
      <label>Choice A</label>
      *choice0*
      <br/>
      <div>
        <label>Image A</label>
        *image0*
      </div>
    </div>
    <div>
      <label>Choice B</label>
      *choice1*
      <br/>
      <div>
        <label>Image B</label>
        *image1*
      </div>
    </div>
  </div>
";

$placeholders = array();
$placeholders['status'] =
"
  <select name='status'>
    <option value='1'>TERBUKA</option>
    <option value='0'>SUDAH SELESAI</option>
  </select>
";

$placeholders['deadline']= "<input type='text' class='date' name='deadline' value='' />";
$placeholders['choice0'] = "<input type='text' name='choice[]' />";
$placeholders['choice1'] = "<input type='text' name='choice[]' />";
$placeholders['image0']  = $placeholders['image1']  = "
  <input type='hidden' name='images[]' />
  <input type='file' class='debate-campaign-upload'>
  <span class='debate-campaign-notif hidden' style='color:red'><br/>please wait..<br/></span>
  <img />
";

if (!is_null ($campaign)) {
  $placeholders['status']   = $campaign->status == 0 ? 
    str_replace("<option value='0'>SUDAH SELESAI</option>", "<option value='0' selected='selected'>SUDAH SELESAI</option>", $placeholders['status']):
    str_replace("<option value='1'>TERBUKA</option>", "<option value='1' selected='selected'>TERBUKA</option>", $placeholders['status']);
  $placeholders['deadline'] = "<input type='text' class='date' name='deadline' value='$campaign->deadline' />";
  for ($c = 0; $c <= 1; $c++) {
    if (isset ($campaign->choice[$c])) {
      $choice = $campaign->choice[$c];
      $placeholders["choice$c"] = "<input type='text' name='choice[$choice->id]' value='$choice->title' />";
      $placeholders["image$c"] = "
        <input type='hidden' name='images[$choice->id]' value='$choice->image' />
        <input type='file' class='debate-campaign-upload'>
        <span class='debate-campaign-notif hidden' style='color:red'><br/>please wait..<br/></span>
        <img src='$choice->thumb' />
      ";
    }
  }
}

foreach ($placeholders as $key => $value)
  $structure = str_replace ("*$key*", $value, $structure);

$script = '<script type="text/javascript" src="{{src}}"></script>';
$jquery = '<script type="text/javascript">{{script}}</script>';
$link   = '<link rel="stylesheet" type="text/css" href="{{href}}">';
$debate_campaign_upload_url = DEBATE_CAMPAIGN_LOCAL_API . 'upload';
echo str_replace('{{href}}', DEBATE_CAMPAIGN_URL . 'lib/jquery-ui.datepicker.min.css', $link);
echo str_replace('{{src}}', DEBATE_CAMPAIGN_URL . 'lib/jquery-ui.datepicker.min.js', $script);
echo str_replace('{{script}}', "
  jQuery(function () {
    jQuery('.date').datepicker({ dateFormat: 'yy-mm-dd' })
    jQuery('.debate-campaign-upload').change(function (event) {
      var input = jQuery(this)
      var data  = new FormData()
      data.append('debate-campaign-upload', input[0].files[0])
      data.append('post_ID', $post->ID)
      input.next('.debate-campaign-notif').removeClass('hidden')
      jQuery.ajax({
        url: '$debate_campaign_upload_url',
        type: 'POST', data: data,
        cache: false, dataType: 'json',
        processData: false, contentType: false,
        success: function (attachment_id) {
          input.siblings('input').val(attachment_id)
          jQuery('.debate-campaign-notif').addClass('hidden')
        }
      })
    })
  })
", $jquery);