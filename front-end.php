<?php

$layout = "
  <style>
    .text-center{text-align: center}
    .text-right{text-align: right}
    .text-green{color: green}
    .text-red{color: red}
    .debate-campaign-vote-comment p {padding-left: 20px}
    [class^='debate-campaign-comment-'] {vertical-align: top}
    .has-number.active {font-weight: bold}
    .pad-5-10{padding: 5px 10px}
    .debate-campaign-images{height: 400px; width: 400px; max-width: 400px}
    .ui-dialog-titlebar {display: none}
  </style>
  <table class='debate-campaign-header'>
    <thead class='menu-background'>
      <tr>
        <th class='pad-5-10'>Status Vote: *status*</th>
        <th class='pad-5-10 text-right'>Deadline: *deadline*</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan='2'>*content*</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td class='text-center'>*choice0*</td>
        <td class='text-center'>*choice1*</td>
      </tr>
      <tr>
        <td class='text-center'>*img-choice0*</td>
        <td class='text-center'>*img-choice1*</td>
      </tr>
      <tr>
        <td class='text-center'><button class='debate-campaign-vote mt-radius' data-choice='*id-choice0*'>PILIH</button></td>
        <td class='text-center'><button class='debate-campaign-vote mt-radius' data-choice='*id-choice1*'>PILIH</button></td>
      </tr>
      <tr>
        <td class='text-center'><h2 class='debate-campaign-vote-count'>*vote-count-choice0*</h2></td>
        <td class='text-center'><h2 class='debate-campaign-vote-count'>*vote-count-choice1*</h2></td>
      </tr>
      <tr>
        <td>Komentar (*vote-count-choice0*)</td>
        <td>Komentar (*vote-count-choice1*)</td>
      </tr>
      <tr>
        <td class='debate-campaign-comment-0 commentlist' width='50%'></td>
        <td class='debate-campaign-comment-1 commentlist'></td>
      </tr>
      <tr>
        <td>*vote-page-choice0*</td>
        <td>*vote-page-choice1*</td>
      </tr>
    </tfoot>
  </table>
";

if (!is_null ($campaign)) {
  $campaign->deadline   = date_create("$campaign->deadline");
  $campaign->deadline   = date_format($campaign->deadline, "d/m/Y");
  $layout = str_replace('*status*', $campaign->status == 0 ? 'Sudah Selesai' : 'Terbuka', $layout);
  $layout = str_replace('*deadline*', $campaign->deadline, $layout);
  if (0 == $campaign->status) $layout = str_replace('VOTE', 'SELESAI', $layout);

  foreach ($campaign->choice as $index => $choice) {
    $layout = str_replace("*choice$index*", $choice->title, $layout);
    $layout = str_replace("*img-choice$index*", "<img src='$choice->thumb' class='debate-campaign-images'>", $layout);
    $layout = str_replace("*id-choice$index*", $choice->id, $layout);
    $layout = str_replace("*vote-count-choice$index*", count($choice->vote), $layout);

    $comment_per_page = 10;
    $page_numbers     = '';
    $total_comment    = count($choice->vote);
    $page_index       = 1;
    $page_numbers    .= "<a data-page-choice-index='$index'><<</a>&nbsp;";
    $page_numbers    .= "<a data-page-choice-index='$index'><</a>&nbsp;";
    while ($total_comment > 0) {
      $page_numbers  .= "<a data-page-choice-index='$index' class='has-number'>$page_index</a>&nbsp;";
      $total_comment -= $comment_per_page;
      $page_index++;
    }
    $page_numbers    .= "<a data-page-choice-index='$index'>></a>&nbsp;";
    $page_numbers    .= "<a data-page-choice-index='$index'>>></a>&nbsp;";
    $layout = str_replace("*vote-page-choice$index*", $page_numbers, $layout);
  }
}

$modal = "
<div id='dialog-form' title='Submit Vote'>
  <input type='hidden' id='debate-campaign-input-choice' />
  <label>Nama Lengkap</label>
  <input type='text' id='debate-campaign-input-name'>
  <label>Email</label>
  <input type='text' id='debate-campaign-input-email'>
  <label>No. Handphone (Optional)</label>
  <input type='text' id='debate-campaign-input-tlp'>
  <label>Kota</label>
  <input type='text' id='debate-campaign-input-city'>
  <label>Isi Komentar/Opini/Alasan</label>
  <textarea id='debate-campaign-input-comment'></textarea>
  <small>*semua harus diisi</small>
</div>
";
$layout .= $modal;

$link     = DEBATE_CAMPAIGN_URL . 'lib/jquery-ui.dialog.min.css';
$link     = "<link rel='stylesheet' type='text/css' href='$link'>";
$script   = DEBATE_CAMPAIGN_URL . 'lib/jquery-ui.dialog.min.js';
$script   = "<script type='text/javascript' src='$script'></script>";
$dialog   = DEBATE_CAMPAIGN_URL . 'lib/jquery.dialogOptions.js';
$dialog   = "<script type='text/javascript' src='$dialog'></script>";
$campaign = json_encode($campaign);
$rest     = DEBATE_CAMPAIGN_LOCAL_API;
$rest     = "
  <script type='text/javascript'>
    var debate_campaign_local_api = '$rest'
    var campaign = $campaign
  </script>";
$local    = DEBATE_CAMPAIGN_URL . 'debate-campaign.js';
$local    = "<script type='text/javascript' src='$local'></script>";

$layout  .= $link . $script . $dialog . $rest . $local;