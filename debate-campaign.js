jQuery(function () {
  var vote_modal = jQuery( '#dialog-form' ).dialog({
    autoOpen: false,
    modal: true,
    width: '45%',
    create: function () {
      jQuery('.ui-dialog-buttonset').children('button').removeClass('ui-button')
    },
    buttons: {
      'Submit': function () {
        var param   = {}
        var valid   = true
        jQuery('[id^="debate-campaign-input-"]').each(function () {
          var key   = jQuery(this).attr('id').replace('debate-campaign-input-', '')
          var value = jQuery(this).val()
          if (value.length < 1 && key !== 'tlp') {
            valid   = false
            jQuery(this).focus()
          } else param[key] = value
        })
        if (valid) {
          jQuery.post(debate_campaign_local_api + 'vote', param, function () {
            jQuery('[id^="debate-campaign-input-"]').val('')
            window.location.reload()
          })
        }
      },
      Cancel  : function() {
        vote_modal.dialog('close')
      }
    },
    close: function() {
      jQuery('[id^="debate-campaign-input-"]').val('')
    }
  })

  if (campaign.status == 1)
  jQuery('.debate-campaign-vote').click(function () {
    var choiceID = jQuery(this).attr('data-choice')
    jQuery('#debate-campaign-input-choice').val(choiceID)
    vote_modal.dialog('open')
  })

  var vote_count = jQuery('.debate-campaign-vote-count')
  if (parseInt (vote_count.eq(0).html()) > parseInt (vote_count.eq(1).html())) {
    vote_count.eq(0).addClass('text-green')
    vote_count.eq(1).addClass('text-red')
  } else {
    vote_count.eq(1).addClass('text-green')
    vote_count.eq(0).addClass('text-red')
  }

  var perpage   = 10
  var commenttpl= 
  '<article class="comment">\
      <footer class="comment-meta">\
          <div class="comment-author vcard">\
              <img alt="" src="http://0.gravatar.com/avatar/650878b92149c7eafb73339c94e83a1b?s=80&amp;d=mm&amp;r=g" srcset="http://0.gravatar.com/avatar/650878b92149c7eafb73339c94e83a1b?s=160&amp;d=mm&amp;r=g 2x" class="avatar avatar-80 photo" height="80" width="80">\
              <h5 class="fn">vote.name</h5>\
              <a class="mt_comment_date">\
                <time>vote.created</time>\
              </a>\
          </div>\
      </footer>\
      <div class="comment-content">\
          <p>vote.comment</p>\
      </div>\
      <div class="clear"></div>\
  </article>'

  renderComment(0, 1)
  renderComment(1, 1)
  jQuery('[data-page-choice-index]').click(function () {
    var choice  = parseInt(jQuery(this).attr('data-page-choice-index'))
    var page    = parseInt(jQuery(this).html())
    var current = parseInt(jQuery('[data-page-choice-index="'+choice+'"].has-number.active').html())
    var last    = parseInt(jQuery('[data-page-choice-index="'+choice+'"].has-number').last().html())
    if (page > 0) renderComment (choice, page)
    else  switch (jQuery(this).html()) {
      case '&lt;&lt;': renderComment (choice, 1);break
      case '&lt;'    : if (current - 1 > 0) renderComment (choice, current - 1);break
      case '&gt;'    : if (current + 1 <= last) renderComment (choice, current + 1);break
      case '&gt;&gt;': renderComment (choice, last);break
    }
  })

  function renderComment (choice, page) {
    jQuery('.debate-campaign-comment-' + choice).html('')
    var since = page * perpage - perpage
    var until = since + perpage - 1
    for (i = since; i <= until; i++) {
      var vote= campaign.choice[choice].vote[i]
      if (!vote) continue
      comment = commenttpl
        .replace('vote.name', vote.name)
        .replace('vote.created', vote.created)
        .replace('vote.city', vote.city)
        .replace('vote.comment', vote.comment)
      jQuery('.debate-campaign-comment-' + choice).append(comment)
    }

    var page_number = jQuery('[data-page-choice-index="'+choice+'"].has-number')
    page_number.removeClass('active')
    page_number.eq(page-1).addClass('active')
  }
})