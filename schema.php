<?php

  $debateTables         = array();
  $debate_table_campaign= $wpdb->prefix . 'debate_campaign';
  $debate_table_choice  = $wpdb->prefix . 'debate_choice';
  $debate_table_vote    = $wpdb->prefix . 'debate_vote';
  $debateSchema         = array(
    $debate_table_campaign => "
      CREATE TABLE `$debate_table_campaign` (
        `id` int(11) NOT NULL,
        `post_ID` int(11) NOT NULL,
        `post_title` varchar(255) NOT NULL,
        `status` tinyint(4) NOT NULL,
        `deadline` date NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ",
    $debate_table_choice => "
      CREATE TABLE `$debate_table_choice` (
        `id` int(11) NOT NULL,
        `campaign` int(11) NOT NULL,
        `title` varchar(255) NOT NULL,
        `image` varchar(255) NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    ",
    $debate_table_vote => "
      CREATE TABLE `$debate_table_vote` (
        `id` int(11) NOT NULL,
        `ip` varchar(255) NOT NULL,
        `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `choice` int(11) NOT NULL,
        `name` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        `tlp` varchar(255) NOT NULL,
        `city` varchar(255) NOT NULL,
        `comment` text NOT NULL
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    "
  );

  $tables = $debateSchema;
  foreach ($tables as $table => $query) {
    $debateTables[] = $table;
    $debateSchema[$table . 'primary'] = "
      ALTER TABLE `$table` ADD PRIMARY KEY (`id`)
    ";
    $debateSchema[$table . 'increment'] = "
      ALTER TABLE `$table` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
    ";
  }

  $debateFields = array (
    'debate_campaign' => array ('id', 'post_ID', 'post_title', 'status', 'deadline'),
    'debate_choice' => array ('id', 'campaign', 'title', 'image'),
    'debate_vote' => array ('id', 'choice', 'name', 'email', 'tlp', 'city', 'comment')
  );