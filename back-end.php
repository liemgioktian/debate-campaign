<?php

$admin_layout = "
  <div class='wrap'>
    <table class='debate-campaign-admin'>
      <thead>
        <tr>
          <th>NO</th>
          <th>TITLE</th>
          <th>SUBMIT</th>
          <th>NAME</th>
          <th>EMAIL</th>
          <th>TLP</th>
          <th>CITY</th>
          <th>COMMENT</th>
        </tr>
      <thead>
      <tbody>*tbody*</tbody>
    </table>
  </div>
";

$tbody = '';
$no    = 1;
foreach ($records as $record) {
  $tbody .= '<tr>';
  $tbody .= "<td>$no</td>";
  $tbody .= "<td>$record->post_title</td>";
  $tbody .= "<td>$record->title</td>";
  $tbody .= "<td>$record->name</td>";
  $tbody .= "<td>$record->email</td>";
  $tbody .= "<td>$record->tlp</td>";
  $tbody .= "<td>$record->city</td>";
  $tbody .= "<td>$record->comment</td>";
  $tbody .= '</tr>';
  $no++;
}
$admin_layout = str_replace('*tbody*', $tbody, $admin_layout);

echo $admin_layout;