<?php

register_activation_hook( DEBATE_CAMPAIGN_PATH . 'debate-campaign.php', function () {
  global $wpdb;
  include DEBATE_CAMPAIGN_PATH . 'schema.php';
  foreach ($debateSchema as $query) $wpdb->query($query);
});

register_deactivation_hook( DEBATE_CAMPAIGN_PATH . 'debate-campaign.php', function () {
  global $wpdb;
  include DEBATE_CAMPAIGN_PATH . 'schema.php';
  foreach ($debateTables as $table_name) $wpdb->query("DROP TABLE IF EXISTS $table_name;");
});

add_action( 'add_meta_boxes', function () {
  add_meta_box( 'debate-campaign', __( 'Debate Campaign', 'debate-campaign' ), function ($content) {
    global $post;
    $campaign = getDebateCampaign ($post->ID);
    include DEBATE_CAMPAIGN_PATH . 'meta-box.php';
    echo $structure;
  });
});

add_filter( 'content_save_pre' , function ($content) {
  if ($_POST && isset ($_POST['status'])) {
    $campaign = getDebateCampaign($_POST['post_ID']);
    if (isset ($campaign->id)) updateDebateCampaign();
    else insertDebateCampaign();
  }
  return $content;
}, 10, 1);

add_filter('the_content', function ($content) {
  global $post;
  $campaign = getDebateCampaign ($post->ID);
  if (is_null ($campaign)) return $content;
  include DEBATE_CAMPAIGN_PATH . 'front-end.php';
  return str_replace('*content*', $content, $layout);
});

add_action( 'admin_init', function () {
  add_action( 'delete_post', function ($pid) {
    deleteDebateCampaign($pid);
  }, 10 );
});

add_action('rest_api_init', function () {
  register_rest_route( 'debate-campaign', '/vote/', array(
      'methods'   => 'POST',
      'callback'  => 'insertDebateVote',
    )
  );
  register_rest_route( 'debate-campaign', '/upload/', array(
      'methods'   => 'POST',
      'callback'  => function () {
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php'  );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );
        return media_handle_upload( 'debate-campaign-upload', $_POST['post_ID'] );
      },
    )
  );
});

add_action('admin_menu', function () {
  add_menu_page('Debate Campaign', 'Debate Campaign', 'manage_options', 'debate-campaign', function () {
    global $wpdb;
  	$pref= $wpdb->prefix;
  	$q   = "SELECT
       {$pref}debate_campaign.post_title
      ,{$pref}debate_choice.title 
      ,name,email,tlp,city,comment
      FROM {$pref}debate_vote
      JOIN {$pref}debate_choice ON {$pref}debate_vote.choice = {$pref}debate_choice.id
      JOIN {$pref}debate_campaign ON {$pref}debate_choice.campaign = {$pref}debate_campaign.id
    ";

    $records = $wpdb->get_results($q);
    include DEBATE_CAMPAIGN_PATH . 'back-end.php';
    $dtcss= DEBATE_CAMPAIGN_URL . 'lib/jquery.dataTables.min.css';
    $dtjs = DEBATE_CAMPAIGN_URL . 'lib/jquery.dataTables.min.js';
    echo "<link rel='stylesheet' type='text/css' href='$dtcss'>";
    echo "<script type='text/javascript' src='$dtjs'></script>";
    echo "<script type='text/javascript'>jQuery(function (){ jQuery('table.debate-campaign-admin').dataTable() })</script>";
  }, 'dashicons-format-chat');
});

function getDebateCampaign ($postID) {
  global $wpdb;
  $table = $wpdb->prefix . 'debate_campaign';
  $campaign = $wpdb->get_row("SELECT * FROM `$table` WHERE `post_ID` = $postID");
  if (!is_null ($campaign)) {

    $deadline = date($campaign->deadline);
    $today    = date('Y-m-d', time());
    if ($campaign->status != 0 && $deadline <= $today) $campaign->status = 0;

    $table = $wpdb->prefix . 'debate_choice';
    $campaign->choice  = $wpdb->get_results ("SELECT * FROM `$table` WHERE `campaign` = $campaign->id");
    foreach ($campaign->choice as &$choice) {
      $choice->media= wp_get_attachment_url($choice->image);
      $choice->thumb= wp_get_attachment_thumb_url($choice->image);
      $table        = $wpdb->prefix . 'debate_vote';
      $choice->vote = $wpdb->get_results ("SELECT * FROM `$table` WHERE `choice` = $choice->id ORDER BY `created` DESC");
    }
  }
  return $campaign;
}

function insertDebateCampaign () {
  global $wpdb;
  include DEBATE_CAMPAIGN_PATH . 'schema.php';

  $campaign = array();
  foreach ($debateFields['debate_campaign'] as $field) 
    if (isset($_POST[$field])) $campaign[$field] = $_POST[$field];
  $wpdb->insert($wpdb->prefix . 'debate_campaign', $campaign);

  $campaign = getDebateCampaign($_POST['post_ID']);
  foreach ($_POST['choice'] as $index => $title) {
    $choice = array();
    $choice['title']    = $title;
    $choice['image']    = $_POST['images'][$index];
    $choice['campaign'] = $campaign->id;
    $wpdb->insert($wpdb->prefix . 'debate_choice', $choice);
  }
}

function updateDebateCampaign () {
  global $wpdb;
  include DEBATE_CAMPAIGN_PATH . 'schema.php';

  $campaign = array();
  foreach ($debateFields['debate_campaign'] as $field) 
    if (isset($_POST[$field])) $campaign[$field] = $_POST[$field];
  $wpdb->update($wpdb->prefix . 'debate_campaign', $campaign, array('post_ID' => $campaign['post_ID']));

  foreach ($_POST['choice'] as $id => $title) {
    $choice = array (
      'title' => $title,
      'image' => $_POST['images'][$id]
    );
    $wpdb->update($wpdb->prefix . 'debate_choice', $choice, array('id' => $id));
  }
}

function deleteDebateCampaign($id) {
  global $wpdb;
  $campaign = getDebateCampaign ($id);
  if (is_null ($campaign)) return false;
  $wpdb->delete($wpdb->prefix . 'debate_campaign', array('post_ID' => $id));
  $wpdb->delete($wpdb->prefix . 'debate_choice', array('campaign' => $campaign->id));
  foreach ($campaign->choice as $choice) {
    $wpdb->delete($wpdb->prefix . 'debate_vote', array('choice' => $choice->id));
    wp_delete_attachment($choice->image);
  }
}

function insertDebateVote ($req) {
  global $wpdb;
  $table = $wpdb->prefix . 'debate_vote';
  $ip    = getVoterIP();
  //$exists= $wpdb->get_row("SELECT * FROM $table WHERE `ip`='$ip'");
  //if (!is_null ($exists)) return false;
 
  $param = $req->get_params();
  $record= array();
  include DEBATE_CAMPAIGN_PATH . 'schema.php';
  foreach ($debateFields['debate_vote'] as $field)
    if (isset ($param[$field])) $record[$field] = $param[$field];
  $record['ip'] = getVoterIP();
  return $wpdb->insert($table, $record);
}

function getVoterIP () {
  $client  = @$_SERVER['HTTP_CLIENT_IP'];
  $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
  $remote  = $_SERVER['REMOTE_ADDR'];

  if (filter_var ($client, FILTER_VALIDATE_IP)) return $client;
  else if (filter_var ($forward, FILTER_VALIDATE_IP)) return $forward;
  else return $remote;
}

