<?php 
/*
Plugin Name: Debate Campaign
URI: http://www.000software.com
Description: Plugin Debate Campaign
Author: 000software 
Version: 1.2
Author URI: http://www.000software.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

define('DEBATE_CAMPAIGN_PATH',  plugin_dir_path( __FILE__ ));
define('DEBATE_CAMPAIGN_URL', plugin_dir_url(__FILE__));
define('DEBATE_CAMPAIGN_LOCAL_API', site_url('wp-json/debate-campaign/'));

include DEBATE_CAMPAIGN_PATH . 'core.php';